from django.shortcuts import render

# Create your views here.

def appstory(request):
    context = {
        'host' : request.get_host(),
    }
    return render(request, 'appstory/index.html', context)
