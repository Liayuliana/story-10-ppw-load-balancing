from django.urls import path
from . import views

app_name = 'appstory'

urlpatterns = [
    path('', views.appstory, name="appstory"),
]