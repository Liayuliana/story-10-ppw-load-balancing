from django.apps import AppConfig


class AppstoryConfig(AppConfig):
    name = 'appstory'
