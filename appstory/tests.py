from django.test import TestCase, Client
from django.urls import reverse, resolve

# Create your tests here.
class AppStoryUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.appstory = reverse('appstory:appstory')

    #Test urls
    def test_status_url_is_exist(self):
        response =  self.client.get('')
        self.assertEquals(response.status_code, 200)